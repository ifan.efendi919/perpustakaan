import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/assets_constants.dart';
import '../../../data/models/notification_model.dart';
import '../../../utils/services/database_service.dart';
import '../../widgets/custom_text.dart';
import '../screen_layout/screen_layout.dart';
import 'widgets/favorite_card.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenLayout(
        child: StreamBuilder(
      stream: DataBaseService().notification(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data as List<NotificationModel>;
          print.call(data);
          if (data.isEmpty) {
            return SizedBox(
              height: 80.h,
              child: LottieBuilder.asset(
                AssetsConstants.emptyboxlottie,
                height: 50.h,
              ),
            );
          } else {
            return SizedBox(
              height: 80.h,
              child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  NotificationModel notif = data[index];
                  return Container(
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white12,
                      borderRadius: BorderRadius.circular(8.0)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          notif.type
                        )
                      ],
                    ),
                  );
                  // return FavoriteCard(
                  //     img: notif.,
                  //     title: mocvie.title,
                  //     overview: mocvie.overview,
                  //     id: mocvie.movieID);
                },
              ),
            );
          }
        } else {
          return SizedBox(
            height: 70.h,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CustomText(title: "Something Went Wrong !!"),
                SizedBox(
                  height: 2.h,
                ),
                LottieBuilder.asset(
                  AssetsConstants.errorlottie,
                  height: 50.h,
                ),
              ],
            ),
          );
        }
      },
    ));
  }
}
