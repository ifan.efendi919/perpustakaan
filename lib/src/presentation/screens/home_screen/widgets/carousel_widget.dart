import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:perpustakaan/src/data/models/book_model.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../../../../config/app_route.dart';
import 'carousel_item.dart';

class CarouselWidget extends StatelessWidget {
  final List<BookModel> list;
  const CarouselWidget({
    Key? key,
    required this.list,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
        items: list
            .sublist(0, 5)
            .map(
              (item) => CarouselItem(
                  avatar: item.cover,
                  title: item.bookTitle,
                  genre: [1,2],
                  onTapList: () {
                    // String userID =
                    //     Supabase.instance.client.auth.currentUser!.id;
                    // FavoriteMocvie book = FavoriteMocvie(
                    //     movieID: item.id,
                    //     posterPath: item.cover,
                    //     title: item.bookTitle,
                    //     overview: item.bookTitle,
                    //     userID: userID);
                    //
                    // DataBaseService().addMovie(mocvie: book);
                  },
                  onTap: () {
                    context.push(AppRoute.details, extra: item.id);
                  }),
            )
            .toList(),
        options: CarouselOptions(
          aspectRatio: 1 / 0.8,
          autoPlay: true,
          viewportFraction: 1,
          autoPlayAnimationDuration: const Duration(seconds: 2),
          enlargeCenterPage: false,
        ));
  }
}
