import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perpustakaan/src/utils/extensions/extensions.dart';
import 'package:sizer/sizer.dart';

import '../../../blocs/Auth/authentication_bloc.dart';
import '../../../config/app_route.dart';
import '../../../constants/app_colors.dart';
import '../../../constants/assets_constants.dart';
import '../../../utils/helpers/validator.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_text.dart';
import '../../widgets/custom_textfield.dart';
import '../screen_layout/screen_layout.dart';
import 'widgets/social_card.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  late TextEditingController _emailcontroller;
  late TextEditingController _passcontroller;
  final _formkey = GlobalKey<FormState>();
  bool _isloginscreen = true;
  late AnimationController _controller;
  late final Animation<double> _rotationAnimation;

  @override
  void initState() {
    _emailcontroller = TextEditingController();
    _passcontroller = TextEditingController();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000))
      ..forward();

    _rotationAnimation = Tween<double>(begin: 0, end: 2).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeInOutCubic,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _emailcontroller.dispose();
    _passcontroller = TextEditingController();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AuthenticationBloc blocProvider = BlocProvider.of<AuthenticationBloc>(context);
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        if (state is AuthErrorState) {
          context.showsnackbar(title: state.error);
        } else if(state is AuthSuccessState){
          GoRouter.of(context).go(AppRoute.homescreen);
        }
      },
      child: ScreenLayout(
        child: Center(
          child: SizedBox(
            width: double.infinity,
            height: 100.h,
            child: Form(
              key: _formkey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RotationTransition(
                    turns: _rotationAnimation,
                    child: Image.asset(
                      AssetsConstants.appicon,
                      height: 15.h,
                    ),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  CustomText(
                    title: (_isloginscreen)
                        ? 'Login to Your Account'
                        : 'Create Your Account',
                    size: 17,
                  ),
                  SizedBox(
                    height: 7.h,
                  ),
                  CustomTextfield(
                    isSufix: false,
                    hint: 'Enter your Email',
                    icon: Icons.email,
                    keyboardtype: TextInputType.emailAddress,
                    validator: (value) {
                      return !Validators.isValidEmail(value!)
                          ? 'Enter a valid Email'
                          : null;
                    },
                    textEditingController: _emailcontroller,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomTextfield(
                    isSufix: false,
                    hint: 'Enter your Password',
                    icon: Icons.lock,
                    keyboardtype: TextInputType.text,
                    obscureText: true,
                    validator: (value) {
                      return value!.length < 6 ? 'Enter a valid Password' : null;
                    },
                    textEditingController: _passcontroller,
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  BlocBuilder<AuthenticationBloc, AuthenticationState>(
                    builder: (context, state) {
                      if (state is AuthLoadingState) {
                        return const CircularProgressIndicator(
                          color: Colors.green,
                        );
                      } else {
                        return CustomButton(
                          func: () async {
                            if (_formkey.currentState!.validate()) {
                              if (_isloginscreen) {
                                BlocProvider.of<AuthenticationBloc>(context).add(EmailSignInAuthEvent(_emailcontroller.text, _passcontroller.text));
                              } else {
                                BlocProvider.of<AuthenticationBloc>(context).add(
                                  EmailSignUpAuthEvent(_emailcontroller.text,
                                      _passcontroller.text),
                                );
                              }
                            }
                          },
                          title: (_isloginscreen) ? 'Log In' : 'Sign Up',
                        );
                      }
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        title: (_isloginscreen)
                            ? "Don't have an account ?"
                            : "You already have an account ?",
                        headline: false,
                        size: 9,
                      ),
                      SizedBox(width: 2.w),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _isloginscreen = !_isloginscreen;

                            _controller
                              ..reset()
                              ..forward();
                          });
                        },
                        child: Text(
                          (_isloginscreen) ? 'Sign Up' : 'Log In',
                          style: Theme.of(context).textTheme.displayLarge!.copyWith(
                            color: AppColors.green,
                            fontSize: 10.sp,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
