import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:perpustakaan/src/blocs/detail_book/book_details_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../../blocs/movie_cast/movie_cast_bloc.dart';
import '../../../blocs/movie_details/movie_details_bloc.dart';
import '../../../blocs/movie_images/movie_images_bloc.dart';
import '../../../blocs/movie_video/movie_video_bloc.dart';
import '../../../config/app_route.dart';
import '../../../constants/app_colors.dart';
import '../../../data/models/rent_model.dart';
import '../../../utils/services/database_service.dart';
import '../../widgets/custom_error_widget.dart';
import '../../widgets/custom_text.dart';
import 'widgets/back_button.dart';
import 'widgets/cast_widget.dart';
import 'widgets/custom_flexible_spacebar.dart';
import 'widgets/screenshot_widget.dart';

class DetailsScreen extends StatefulWidget {
  final int id;
  const DetailsScreen({super.key, required this.id});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  late int lama = 0;
  @override
  void initState() {
    context.read<BookDetailsBloc>().add(LoadBookDetails(widget.id));
    // context.read<MovieDetailsBloc>().add(LoadMovieDetails(widget.id));
    // context.read<MovieImagesBloc>().add(LoadMovieImages(widget.id));
    // context.read<MovieCastBloc>().add(LoadMovieCast(widget.id));
    // context.read<MovieVideoBloc>().add(LoadMovieVideo(widget.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return Container();
    return BlocBuilder<BookDetailsBloc, BookDetailsState>(
      builder: (context, bookstate) {
        if (bookstate is BookDetailsLoading) {
          return const Center(
              child: CircularProgressIndicator(
            color: AppColors.green,
          ));
        } else if (bookstate is BookDetailsSuccess) {
          // return Scaffold(body: Container(),);
          print.call(bookstate.rent != null);
          return Scaffold(
              body: CustomScrollView(
            slivers: [
              SliverAppBar(
                pinned: true,
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                leading: const CustomBackButton(),
                flexibleSpace: CustomFlexibleSpaceBar(book: bookstate.book),
                expandedHeight: 65.h,
              ),
              SliverList(
                  delegate: SliverChildListDelegate([
                SizedBox(
                  height: 5.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        maxLines: 15,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          text: 'Overview : ',
                          style:
                              Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: Colors.amber,
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w900,
                                  ),
                          children: [
                            TextSpan(
                              text: bookstate.book.bookTitle,
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                    fontSize: 10.sp,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 2.h),
                      const CustomText(
                        title: 'Description ',
                        size: 12,
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          '${bookstate.book.description}',
                          textAlign: TextAlign.justify,
                        ),
                      ),
                      SizedBox(height: 2.h,),
                      (bookstate.rent != null
                          ? Row(
                        children: [
                          const Expanded(
                              child: Text(
                              'Book return date',
                              style: TextStyle(
                                  color: Colors.white
                              ),
                            )
                          ),
                          Text(
                            bookstate.rent!.tanggalPengembalian,
                            style: const TextStyle(
                              color: Colors.white
                            ),
                          )
                        ],
                      )
                          : Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Expanded(
                                  child: Text(
                                      'Book borrowing time'
                                  )
                              ),
                              GestureDetector(
                                onTap: (){
                                  if(lama > 0){
                                    setState(() {
                                      lama = lama-1;
                                    });
                                  }
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(8.0),
                                          topLeft: Radius.circular(8.0)
                                      )
                                  ),
                                  child: const Icon(
                                      Icons.remove
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 20),
                                decoration: const BoxDecoration(
                                    color: Colors.white
                                ),
                                child: Text(
                                  lama.toString(),
                                  style: const TextStyle(
                                      color: AppColors.black
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    lama = lama+1;
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(8.0),
                                          topRight: Radius.circular(8.0)
                                      )
                                  ),
                                  child: const Icon(
                                      Icons.add
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 2.h),
                          GestureDetector(
                            onTap: (){
                              if(lama > 0){
                                String userID = Supabase.instance.client.auth.currentUser!.id;
                                RentBook rent = RentBook(
                                    tanggal: DateFormat('yyyy-MM-dd').format(DateTime.now()).toString(),
                                    bukuId: bookstate.book.id,
                                    anggotaId: userID,
                                    tanggalPeminjaman: DateFormat('yyyy-MM-dd').format(DateTime.now()),
                                    tanggalPengembalian: DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: lama))).toString(),
                                    createdBy: userID,
                                    createdTime: DateFormat('yyyy-MM-dd H:m:s').format(DateTime.now()).toString(),
                                    lamaPinjaman: lama
                                );

                                DataBaseService().addRent(rent: rent);
                              }else{
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Determine the length of borrowing the book"),
                                    )
                                );
                                // Fluttertoast.showToast(
                                //     msg: "Determine the length of borrowing the book",
                                //     toastLength: Toast.LENGTH_SHORT,
                                //     gravity: ToastGravity.CENTER,
                                //     timeInSecForIosWeb: 1,
                                //     backgroundColor: Colors.red,
                                //     textColor: Colors.white,
                                //     fontSize: 16.0
                                // );
                              }
                              // context.push(AppRoute.rent, extra: widget.id);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(8.0)
                              ),
                              child: const Text(
                                'Rent Book',
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                      ),
                      SizedBox(height: 5.h),
                    ],
                  ),
                ),
              ]))
            ],
          ));
        } else if (bookstate is BookDetailsError) {
          return CustomErrorWidget(
            error: bookstate.error,
            func: () {
              context.read<BookDetailsBloc>().add(LoadBookDetails(widget.id));
              // context.read<MovieDetailsBloc>().add(LoadMovieDetails(widget.id));
              // context.read<MovieImagesBloc>().add(LoadMovieImages(widget.id));
              // context.read<MovieCastBloc>().add(LoadMovieCast(widget.id));
              // context.read<MovieVideoBloc>().add(LoadMovieVideo(widget.id));
            },
          );
        }
    //
        return const SizedBox.shrink();
      },
    );
  }
}
