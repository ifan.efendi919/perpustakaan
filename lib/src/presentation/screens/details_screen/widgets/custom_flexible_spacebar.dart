import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perpustakaan/src/data/models/book_model.dart';
import 'package:perpustakaan/src/utils/extensions/extensions.dart';
import 'package:sizer/sizer.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../../../blocs/movie_video/movie_video_bloc.dart';
import '../../../../config/app_route.dart';
import '../../../../constants/app_colors.dart';
import '../../../../data/core/api_constants.dart';
import '../../../../data/models/favorite_movie_model.dart';
import '../../../../data/models/movie_details_model.dart';
import '../../../../utils/services/database_service.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/custom_text.dart';
import 'favorite_button.dart';

class CustomFlexibleSpaceBar extends StatefulWidget {
  final BookModel book;

  const CustomFlexibleSpaceBar({super.key, required this.book});
  @override
  CustomFlexibleSpaceBarState createState() {
    return CustomFlexibleSpaceBarState();
  }
}

class CustomFlexibleSpaceBarState extends State<CustomFlexibleSpaceBar> {
  ScrollPosition? _position;
  bool? _visible;

  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _removeListener();
    _addListener();
  }

  void _addListener() {
    _position = Scrollable.of(context).position;
    _position?.addListener(_positionListener);
    _positionListener();
  }

  void _removeListener() {
    _position?.removeListener(_positionListener);
  }

  void _positionListener() {
    final FlexibleSpaceBarSettings? settings =
        context.dependOnInheritedWidgetOfExactType<FlexibleSpaceBarSettings>();
    bool visible =
        settings == null || settings.currentExtent <= settings.minExtent;
    if (_visible != visible) {
      setState(() {
        _visible = visible;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return FlexibleSpaceBar(
      centerTitle: true,
      expandedTitleScale: 1,
      background: Stack(
        fit: StackFit.expand,
        children: [
          CachedNetworkImage(
            imageUrl:
                widget.book.cover,
            fit: BoxFit.cover,
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Theme.of(context).scaffoldBackgroundColor.withOpacity(0.3),
                Theme.of(context).scaffoldBackgroundColor,
              ],
            )),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomText(
                title: widget.book.bookTitle,
                maxlines: 4,
                horizontalpadding: 20,
              ),
              SizedBox(height: 2.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildCoulmnInfo(
                    title: 'Year',
                    content: widget.book.publicationDate.split('-')[0],
                  ),
                ],
              ),
              SizedBox(height: 2.h),
              // BlocBuilder<MovieVideoBloc, MovieVideoState>(
              //   builder: (context, state) {
              //     if (state is MovieVideoSuccess) {
              //       return Row(
              //         mainAxisAlignment: MainAxisAlignment.center,
              //         children: [
              //           SizedBox(
              //             width: 70.w,
              //             child: CustomButton(
              //               func: () {
              //                 context.push(AppRoute.trailer,
              //                     extra: state.videos);
              //               },
              //               title: 'Play Trailer',
              //               shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(5)),
              //             ),
              //           ),
              //           SizedBox(
              //             width: 5.w,
              //           ),
              //           CustomFavoriteButton(
              //             func: () {
              //               User? user =
              //                   Supabase.instance.client.auth.currentUser;
              //               FavoriteMocvie movie = FavoriteMocvie(
              //                 movieID: widget.movie.id,
              //                 posterPath: widget.movie.posterPath,
              //                 title: widget.movie.title,
              //                 overview: widget.movie.overview,
              //                 userID: user!.id,
              //               );
              //               if (user.email != 'omar@gmail.com') {
              //                 DataBaseService()
              //                     .addMovie(mocvie: movie)
              //                     .then((value) {
              //                   context.showsnackbar(
              //                       title: 'Movie Saved Successfully',
              //                       color: AppColors.green);
              //                 }).catchError((e) {
              //                   context.showsnackbar(
              //                       title: e.toString(), color: Colors.red);
              //                 });
              //               } else {
              //                 context.showsnackbar(
              //                     title:
              //                         "Sorry, but you can't save movies as an anonymous user !\n--> Please Log In with your email so you can save movies.",
              //                     color: Colors.red);
              //               }
              //             },
              //           )
              //         ],
              //       );
              //     }
              //     return const SizedBox.shrink();
              //   },
              // )
            ],
          )
        ],
      ),
      title: Visibility(
        visible: (_visible ?? false),
        child: const CustomText(
          title: 'Detail Book',
          color: AppColors.green,
        ),
      ),
    );
  }

  Column _buildCoulmnInfo({required String title, required String content}) {
    return Column(
      children: [
        CustomText(
          title: title,
          color: Colors.grey,
          size: 8,
        ),
        SizedBox(
          height: 1.h,
        ),
        CustomText(
          title: content,
          size: 10,
        ),
      ],
    );
  }
}
