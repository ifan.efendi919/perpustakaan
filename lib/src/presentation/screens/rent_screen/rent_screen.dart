import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:sizer/sizer.dart';

import '../../../blocs/detail_book/book_details_bloc.dart';
import '../../../config/app_route.dart';
import '../../../constants/app_colors.dart';
import '../../widgets/custom_error_widget.dart';
import '../../widgets/custom_text.dart';
import '../details_screen/widgets/back_button.dart';
import '../details_screen/widgets/custom_flexible_spacebar.dart';

class RentScreen extends StatefulWidget {
  final int id;
  const RentScreen({super.key, required this.id});

  @override
  State<RentScreen> createState() => _RentScreenState();
}

class _RentScreenState extends State<RentScreen> {

  @override
  void initState() {
    context.read<BookDetailsBloc>().add(LoadBookDetails(widget.id));
    // context.read<MovieDetailsBloc>().add(LoadMovieDetails(widget.id));
    // context.read<MovieImagesBloc>().add(LoadMovieImages(widget.id));
    // context.read<MovieCastBloc>().add(LoadMovieCast(widget.id));
    // context.read<MovieVideoBloc>().add(LoadMovieVideo(widget.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BookDetailsBloc, BookDetailsState>(
      builder: (context, bookstate) {
        if (bookstate is BookDetailsLoading) {
          return const Center(
              child: CircularProgressIndicator(
                color: AppColors.green,
              ));
        } else if (bookstate is BookDetailsSuccess) {
          // return Scaffold(body: Container(),);
          return Scaffold(
              body: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    pinned: true,
                    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                    leading: const CustomBackButton(),
                    flexibleSpace: CustomFlexibleSpaceBar(book: bookstate.book),
                    expandedHeight: 65.h,
                  ),
                  SliverList(
                      delegate: SliverChildListDelegate([
                        SizedBox(
                          height: 5.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                maxLines: 15,
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                  text: 'Overview : ',
                                  style:
                                  Theme.of(context).textTheme.titleMedium!.copyWith(
                                    color: Colors.amber,
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w900,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: bookstate.book.bookTitle,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium!
                                          .copyWith(
                                        fontSize: 10.sp,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 2.h),
                              const CustomText(
                                title: 'Description ',
                                size: 12,
                              ),
                              Container(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  '${bookstate.book.description}',
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              SizedBox(height: 2.h),
                              GestureDetector(
                                onTap: (){
                                  context.push(AppRoute.rent, extra: widget.id);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                                  decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(8.0)
                                  ),
                                  child: const Text(
                                    'Rent Book',
                                    style: TextStyle(
                                        color: Colors.white
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 5.h),
                            ],
                          ),
                        ),
                      ]))
                ],
              )
          );
        } else if (bookstate is BookDetailsError) {
          return CustomErrorWidget(
            error: bookstate.error,
            func: () {
              context.read<BookDetailsBloc>().add(LoadBookDetails(widget.id));
              // context.read<MovieDetailsBloc>().add(LoadMovieDetails(widget.id));
              // context.read<MovieImagesBloc>().add(LoadMovieImages(widget.id));
              // context.read<MovieCastBloc>().add(LoadMovieCast(widget.id));
              // context.read<MovieVideoBloc>().add(LoadMovieVideo(widget.id));
            },
          );
        }

        return const SizedBox.shrink();
      }
    );
  }
}
