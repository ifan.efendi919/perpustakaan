
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:perpustakaan/src/data/models/book_model.dart';
import 'package:perpustakaan/src/data/models/notification_model.dart';
import 'package:perpustakaan/src/data/models/rent_model.dart';

import '../data_sources/local_data_source.dart';
import '../data_sources/remote_data_source.dart';

abstract class BookRepository {
  Future<List<BookModel>> getListBook({required int page});
  Future<BookModel> getBookDetails({required int id});
  Future<RentBook?> getRentBook({required int id});
  Future<List<BookModel>> getSearchBook({required String query});
  Future<List<NotificationModel>> getNotification({required int page});
  // Future<List<MovieModel>> getTrending({required int page});
  // Future<List<MovieModel>> getUpcoming({required int page});
  // Future<List<MovieModel>> getSearchedMovie({required String query});
  // Future<MovieDetailsModel> getMovieDetails({required int id});
  // Future<List<dynamic>> getImages({required int id});
  // Future<List<CastCrewModel>> getCastCrew({required int id});
  // Future<List<VideoModel>> getVideo({required int id});
}

class BookRepoImpl implements BookRepository {
  final RemoteDataSource remotedatasource;
  final LocalDataSourceBook localdatasource;
  final InternetConnectionChecker internetConnectionChecker;

  BookRepoImpl(this.remotedatasource, this.localdatasource,
      this.internetConnectionChecker);
  @override
  Future<List<BookModel>> getListBook({required int page}) async {
    try {
      if (await internetConnectionChecker.hasConnection) {
        final books = await remotedatasource.getListBook(page: page);

        await localdatasource.cache(
            key: cachedListBook, books: books, page: page);
        return books;
      } else {
        final cachedBooks =await localdatasource.getBook(key: cachedListBook, page: page);
        return cachedBooks;
      }
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<BookModel> getBookDetails({required int id}) async {
    try {
      final books = await remotedatasource.getBookDetails(id: id);
      return books;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<RentBook?> getRentBook({required int id}) async {
    try {
      final rent = await remotedatasource.getRentBook(id: id);
      return rent;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<BookModel>> getSearchBook({required String query}) async {
    try {
      final books = await remotedatasource.getSearchBook(query: query);
      return books;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<NotificationModel>> getNotification({required int page}) async {
    try {
      final notif = await remotedatasource.getNotification(page: page);
      return notif;
    } catch (_) {
      rethrow;
    }
  }

  // @override
  // Future<List<MovieModel>> getTrending({required int page}) async {
  //   try {
  //     if (await internetConnectionChecker.hasConnection) {
  //       final movies = await remotedatasource.getTrending(page: page);
  //
  //       await localdatasource.cache(
  //           key: cachedTrendingMovies, movies: movies, page: page);
  //       return movies;
  //     } else {
  //       final cachedmovies =
  //       await localdatasource.get(key: cachedTrendingMovies, page: page);
  //       return cachedmovies;
  //     }
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<List<MovieModel>> getUpcoming({required int page}) async {
  //   try {
  //     if (await internetConnectionChecker.hasConnection) {
  //       final movies = await remotedatasource.getUpcoming(page: page);
  //
  //       await localdatasource.cache(
  //           key: cachedUpcomingMovies, movies: movies, page: page);
  //       return movies;
  //     } else {
  //       final cachedmovies =
  //       await localdatasource.get(key: cachedUpcomingMovies, page: page);
  //       return cachedmovies;
  //     }
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<List<MovieModel>> getSearchedMovie({required String query}) async {
  //   try {
  //     final movies = await remotedatasource.getSearchedMovie(query: query);
  //     return movies;
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<MovieDetailsModel> getMovieDetails({required int id}) async {
  //   try {
  //     final movies = await remotedatasource.getMovieDetails(id: id);
  //     return movies;
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<List<dynamic>> getImages({required int id}) async {
  //   try {
  //     final images = await remotedatasource.getImages(id: id);
  //     return images;
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<List<CastCrewModel>> getCastCrew({required int id}) async {
  //   try {
  //     final cast = await remotedatasource.getCastCrew(id: id);
  //     return cast;
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
  //
  // @override
  // Future<List<VideoModel>> getVideo({required int id}) async {
  //   try {
  //     final videos = await remotedatasource.getVideo(id: id);
  //     return videos;
  //   } catch (_) {
  //     rethrow;
  //   }
  // }
}