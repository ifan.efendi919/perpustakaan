import 'dart:convert';
import 'package:hive/hive.dart';

import '../models/book_model.dart';
import '../models/movie_model.dart';

const String cachedPopularMovies = 'CACHED_CHARACTERS';
const String cachedListBook = 'CACHED_CHARACTERS';
const String cachedTrendingMovies = 'CACHED_EPISODES';
const String cachedUpcomingMovies = 'CACHED_LOCATIONS';
const String hiveBox = 'MY_MOVIE_BOX';

abstract class LocalDataSource {
  Future<void> cache(
      {required String key,
      required List<MovieModel> movies,
      required int page});
  Future<List<MovieModel>> get({required String key, required int page});
}

class LocalDataSourceImpl implements LocalDataSource {
  @override
  Future<void> cache(
      {required String key,
      required List<MovieModel> movies,
      required int page}) async {
    if (page == 1) {
      final movieBox = await Hive.openBox(hiveBox);
      return movieBox.put(
          key, json.encode(movies.map((e) => e.toJson()).toList()));
    }
  }

  @override
  Future<List<MovieModel>> get({required String key, required int page}) async {
    if (page == 1) {
      final movieBox = await Hive.openBox(hiveBox);
      final cachedmovies = movieBox.get(key);
      if (cachedmovies != null) {
        return json
            .decode(cachedmovies)
            .map<MovieModel>((e) => MovieModel.fromJson(e))
            .toList();
      } else {
        throw 'Empty Cache !!';
      }
    } else {
      return <MovieModel>[];
    }
  }
}

abstract class LocalDataSourceBook {
  Future<void> cache(
      {required String key,
        required List<BookModel> books,
        required int page});
  Future<List<BookModel>> getBook({required String key, required int page});
  Future<BookModel> getBookDetails({required String key, required int id});
}

class LocalDataSourceBookImpl implements LocalDataSourceBook {
  @override
  Future<void> cache(
      {required String key,
        required List<BookModel> books,
        required int page}) async {
    if (page == 1) {
      final bookBox = await Hive.openBox(hiveBox);
      return bookBox.put(
          key, json.encode(books.map((e) => e.toJson()).toList()));
    }
  }

  @override
  Future<List<BookModel>> getBook({required String key, required int page}) async {
    if (page == 1) {
      final bookBox = await Hive.openBox(hiveBox);
      final cachedBooks = bookBox.get(key);
      if (cachedBooks != null) {
        return json
            .decode(cachedBooks)
            .map<BookModel>((e) => BookModel.fromJson(e))
            .toList();
      } else {
        throw 'Empty Cache !!';
      }
    } else {
      return <BookModel>[];
    }
  }

  Future<BookModel> getBookDetails({required String key, required int id}) async {
    final bookBox = await Hive.openBox(hiveBox);
    final cachedBooks = bookBox.get(key);
    if (cachedBooks != null) {
      return BookModel.fromJson(jsonDecode(cachedBooks));
    } else {
      throw 'Empty Cache !!';
    }
  }
}
