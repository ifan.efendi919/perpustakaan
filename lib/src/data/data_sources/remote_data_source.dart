import 'package:intl/intl.dart';
import 'package:perpustakaan/src/data/models/notification_model.dart';
import 'package:perpustakaan/src/data/models/rent_model.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../core/dio_client.dart';
import '../models/book_model.dart';
import '../models/cast_model.dart';
import '../models/movie_details_model.dart';
import '../models/movie_model.dart';
import '../models/video_model.dart';

abstract class RemoteDataSource {
  Future<List<MovieModel>> getPopular({required int page});
  Future<List<MovieModel>> getTrending({required int page});
  Future<List<MovieModel>> getUpcoming({required int page});
  Future<List<MovieModel>> getSearchedMovie({required String query});
  Future<MovieDetailsModel> getMovieDetails({required int id});
  Future<List<dynamic>> getImages({required int id});
  Future<List<CastCrewModel>> getCastCrew({required int id});
  Future<List<VideoModel>> getVideo({required int id});

  Future<List<BookModel>> getListBook({required int page});
  Future<BookModel> getBookDetails({required int id});
  Future<RentBook?> getRentBook({required int id});
  Future<List<BookModel>> getSearchBook({required String query});
  Future<List<NotificationModel>> getNotification({required int page});
}

class RemoteDataSourceImpl implements RemoteDataSource {
  final DioClient _dioClient;

  RemoteDataSourceImpl(this._dioClient);

  @override
  Future<List<MovieModel>> getPopular({required int page}) async {
    try {
      final response = await _dioClient.get(page: page, path: 'top_rated');
      final movies = response.data['results']
          .map<MovieModel>((json) => MovieModel.fromJson(json))
          .toList();
      return movies;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<MovieModel>> getTrending({required int page}) async {
    try {
      final response = await _dioClient.get(page: page, path: 'popular');
      final movies = response.data['results']
          .map<MovieModel>((json) => MovieModel.fromJson(json))
          .toList();
      return movies;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<MovieModel>> getUpcoming({required int page}) async {
    try {
      final response = await _dioClient.get(page: page, path: 'upcoming');
      final movies = response.data['results']
          .map<MovieModel>((json) => MovieModel.fromJson(json))
          .toList();
      return movies;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<MovieModel>> getSearchedMovie({required String query}) async {
    try {
      final response = await _dioClient.search(query: query);
      final movies = response.data['results']
          .map<MovieModel>((json) => MovieModel.fromJson(json))
          .toList();
      return movies;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<MovieDetailsModel> getMovieDetails({required int id}) async {
    try {
      final response = await _dioClient.getDetails(id: id);
      final movies = MovieDetailsModel.fromMap(response.data);

      return movies;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<dynamic>> getImages({required int id}) async {
    try {
      final response = await _dioClient.getImages(id: id);
      final images = response.data['backdrops'];

      return images;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<CastCrewModel>> getCastCrew({required int id}) async {
    try {
      final response = await _dioClient.getcastcrew(id: id);
      final cast = response.data['cast']
          .map<CastCrewModel>((json) => CastCrewModel.fromJson(json))
          .toList();

      return cast;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<VideoModel>> getVideo({required int id}) async {
    try {
      final response = await _dioClient.getvideo(id: id);
      final cast = response.data['results']
          .map<VideoModel>((json) => VideoModel.fromJson(json))
          .toList();

      return cast;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<BookModel>> getListBook({required int page}) async {
    try {
      final supabase = Supabase.instance.client;
      final data = await supabase.from('library_books')
          .select('*')
          .range(page,5);
      final books = data.map<BookModel>((json) => BookModel.fromJson(json))
          .toList();
      return books;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<BookModel> getBookDetails({required int id}) async {
    try {
      final supabase = Supabase.instance.client;
      final data = await supabase.from('library_books')
          .select('*').eq('id', id).single();
      final books = BookModel.fromJson(data);
      return books;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<RentBook?> getRentBook({required int id}) async {
    try {
      final supabase = Supabase.instance.client;
      final data = await supabase.from('peminjaman')
          .select('*').eq('buku_id', id).eq('anggota_id', supabase.auth.currentUser!.id).gte('tanggal_pengembalian', DateFormat('yyyy-MM-dd').format(DateTime.now()));

      if(data.isEmpty){
        return null;
      }
      final rent = RentBook.fromJson(data[0]);

      return rent;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<BookModel>> getSearchBook({required String query}) async {
    try {
      final supabase = Supabase.instance.client;
      final data = await supabase.from('library_books')
          .select('*').ilike('book_title', '%$query%');
      final books = data.map<BookModel>((json) => BookModel.fromJson(json))
          .toList();
      return books;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<NotificationModel>> getNotification({required int page}) async {
    try {
      final supabase = Supabase.instance.client;
      final data = await supabase.from('notification')
          .select('*').range(page,5);
      final notif = data.map<NotificationModel>((json) => NotificationModel.fromJson(json))
          .toList();
      return notif;
    } catch (_) {
      rethrow;
    }
  }
}
