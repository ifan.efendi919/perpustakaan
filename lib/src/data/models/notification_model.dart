import 'package:equatable/equatable.dart';

class NotificationModel extends Equatable {
  final int id;
  final int peminjamanId;
  final String? pengirim;
  final String? penerima;
  final String? pesan;
  final String type;
  final bool dibaca;
  final String tanggalKirim;

  const NotificationModel({
    required this.id,
    required this.peminjamanId,
    required this.pengirim,
    required this.penerima,
    required this.pesan,
    required this.type,
    required this.dibaca,
    required this.tanggalKirim
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
        id: json['id'],
        peminjamanId: json['peminjaman_id'],
        pengirim: json['pengirim'],
        penerima: json['penerima'],
        pesan: json['pesan'],
        type: json['type'],
        dibaca: json['dibaca'],
        tanggalKirim: json['tanggal_kirim']
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'peminjaman_id': peminjamanId,
      'pengirim': pengirim,
      'penerima': penerima,
      'pesan': pesan,
      'type': type,
      'dibaca': dibaca,
      'tanggal_kirim': tanggalKirim
    };
  }

  @override
  List<Object?> get props => [id, pesan];

  @override
  bool? get stringify => true;
}
