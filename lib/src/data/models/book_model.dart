import 'package:equatable/equatable.dart';

class BookModel extends Equatable {
  final String cover;
  final int? id;
  final String bookTitle;
  final String author;
  final String publicationDate;
  final bool availability;
  final String? description;
  final int? jumlah;
  final bool isActive;
  final String? createdBy,createdTime,updatedBy,updatedTime,deletedBy,deletedTime;

  const BookModel({
    required this.cover,
    required this.id,
    required this.bookTitle,
    required this.author,
    required this.publicationDate,
    required this.availability,
    required this.description,
    required this.jumlah,
    required this.isActive,
    required this.createdBy,
    required this.createdTime,
    required this.updatedBy,
    required this.updatedTime,
    required this.deletedBy,
    required this.deletedTime
  });

  factory BookModel.fromJson(Map<String, dynamic> json) {
    return BookModel(
      cover: json['cover'] ?? '',
      id: json['id']?.toInt() ?? 0,
      bookTitle: json['book_title'] ?? '',
      author: json['author'],
      publicationDate: json['publication_date'],
      availability: json['availability'],
      description: json['description'],
      jumlah: json['jumlah'],
      isActive: json['is_active'],
      createdBy: json['created_by'],
      createdTime: json['created_time'],
      updatedBy: json['updated_by'],
      updatedTime: json['updated_time'],
      deletedBy: json['deleted_by'],
      deletedTime: json['deleted_time']
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'book_title': bookTitle,
      'cover': cover,
      'author': author,
      'publication_date': publicationDate,
      'availability': availability,
      'description': description,
      'jumlah': jumlah,
      'is_active': isActive,
      'created_by': createdBy,
      'created_time': createdTime,
      'updated_by': updatedBy,
      'updated_time': updatedTime,
      'deleted_by': deletedBy,
      'deleted_time': deletedTime
    };
  }

  @override
  List<Object?> get props => [id, bookTitle];

  @override
  bool? get stringify => true;
}
