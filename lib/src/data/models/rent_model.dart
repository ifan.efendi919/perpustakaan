class RentBook {
  final String tanggal;
  final int? bukuId;
  final String anggotaId;
  final String tanggalPeminjaman;
  final String tanggalPengembalian;
  final String createdBy;
  final String createdTime;
  final int? lamaPinjaman;

  RentBook(
      {required this.tanggal,
        required this.bukuId,
        required this.anggotaId,
        required this.tanggalPeminjaman,
        required this.tanggalPengembalian,
        required this.createdBy,
        required this.createdTime,
        required this.lamaPinjaman,
      });

  factory RentBook.fromJson(Map<String, dynamic> json) {
    return RentBook(
      tanggal: json['tanggal'],
      bukuId: json['buku_id'],
      anggotaId: json['anggota_id'],
      tanggalPeminjaman: json['tanggal_peminjaman'],
      tanggalPengembalian: json['tanggal_pengembalian'],
      createdBy: json['created_by'],
      createdTime: json['created_time'],
      lamaPinjaman: json['lama_pinjaman'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'tanggal': tanggal,
      'buku_id': bukuId,
      'anggota_id': anggotaId,
      'tanggal_peminjaman': tanggalPeminjaman,
      'tanggal_pengembalian': tanggalPengembalian,
      'created_by': createdBy,
      'created_time': createdTime,
      'lama_pinjaman': lamaPinjaman
    };
  }
}