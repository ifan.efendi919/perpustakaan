import 'package:equatable/equatable.dart';

abstract class ListBookEvent extends Equatable {
  const ListBookEvent();

  @override
  List<Object> get props => [];
}

class LoadListBookEvent extends ListBookEvent {}
