import 'package:equatable/equatable.dart';
import '../../data/models/book_model.dart';


enum ListBookStatus { initial, success, failure }

class ListBookState extends Equatable {
  const ListBookState({
    this.status = ListBookStatus.initial,
    this.listBooks = const <BookModel>[],
    this.hasReachedMax = false,
    this.error = '',
  });

  final ListBookStatus status;
  final List<BookModel> listBooks;
  final bool hasReachedMax;
  final String error;

  ListBookState copyWith({
    ListBookStatus? status,
    List<BookModel>? listBooks,
    bool? hasReachedMax,
    String? error,
  }) {
    return ListBookState(
      status: status ?? this.status,
      listBooks: listBooks ?? this.listBooks,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      error: error ?? this.error,
    );
  }

  @override
  String toString() {
    return '''ListBookState { status: $status, hasReachedMax: $hasReachedMax, ListBooks: ${listBooks.length} }, Error : $error''';
  }

  @override
  List<Object> get props => [status, listBooks, hasReachedMax, error];
}
