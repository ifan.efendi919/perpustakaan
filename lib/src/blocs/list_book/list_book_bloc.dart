import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';
import '../../data/repositories/book_repo_impl.dart';
import 'list_book_event.dart';
import 'list_book_state.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class ListBookBloc extends Bloc<ListBookEvent, ListBookState> {
  final BookRepository _repo;
  int page = 1;
  int limit = 5;

  ListBookBloc(this._repo) : super(const ListBookState()) {
    on<LoadListBookEvent>(
      (event, emit) async {
        if (state.hasReachedMax) return;
        if (state.status == ListBookStatus.initial) {
          try {
            final books = await _repo.getListBook(page: page);

            page = page + 1;
            return emit(const ListBookState().copyWith(
              status: ListBookStatus.success,
              listBooks: books,
              hasReachedMax: false,
            ));
          } catch (e) {
            return emit(const ListBookState().copyWith(
              error: e.toString(),
              status: ListBookStatus.failure,
            ));
          }
        }

        if (page <= limit) {
          try {
            final books = await _repo.getListBook(page: page);
            page = page + 1;
            return emit(const ListBookState().copyWith(
              status: ListBookStatus.success,
              listBooks: List.of(state.listBooks)..addAll(books),
              hasReachedMax: false,
            ));
          } catch (e) {
            return emit(const ListBookState().copyWith(
              error: e.toString(),
              status: ListBookStatus.failure,
            ));
          }
        }

        emit(state.copyWith(hasReachedMax: true));
      },
      transformer: throttleDroppable(throttleDuration),
    );
  }
}
