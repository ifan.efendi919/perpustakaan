part of 'search_book_bloc.dart';

abstract class SearchBookEvent extends Equatable {
  const SearchBookEvent();

  @override
  List<Object> get props => [];
}

class SearchForBookEvent extends SearchBookEvent {
  final String query;

  const SearchForBookEvent({required this.query});

  @override
  List<Object> get props => [query];
}

class RemoveSearchedEvent extends SearchBookEvent {
  const RemoveSearchedEvent();
}
