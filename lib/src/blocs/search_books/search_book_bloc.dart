import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/book_model.dart';
import '../../data/repositories/book_repo_impl.dart';
part 'search_book_event.dart';
part 'search_book_state.dart';

class SearchBookBloc extends Bloc<SearchBookEvent, SearchBookState> {
  final BookRepository _repo;
  SearchBookBloc(this._repo) : super(SearchInitial()) {
    on<SearchForBookEvent>((event, emit) async {
      try {
        emit(SearchLoadingState());
        List<BookModel> movies = await _repo.getSearchBook(query: event.query);
        emit(SearchSuccessState(movies: movies));
      } catch (e) {
        emit(SearchErrorState(message: e.toString()));
      }

      on<RemoveSearchedEvent>((event, emit) {
        emit(SearchInitial());
      });
    });
  }
}
