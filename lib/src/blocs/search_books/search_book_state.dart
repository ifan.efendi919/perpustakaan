part of 'search_book_bloc.dart';

abstract class SearchBookState extends Equatable {
  const SearchBookState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchBookState {}

class SearchLoadingState extends SearchBookState {}

class SearchSuccessState extends SearchBookState {
  final List<BookModel> movies;
  const SearchSuccessState({
    required this.movies,
  });

  @override
  List<Object> get props => [movies];
}

class SearchErrorState extends SearchBookState {
  final String message;
  const SearchErrorState({
    required this.message,
  });

  @override
  List<Object> get props => [message];
}
