import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';
import '../../data/repositories/book_repo_impl.dart';
import '../../data/models/notification_model.dart';
part 'notification_event.dart';
part 'notification_state.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final BookRepository _repo;
  int page = 1;
  int limit = 5;

  NotificationBloc(this._repo) : super(const NotificationState()) {
    on<LoadNotificationEvent>(
          (event, emit) async {
        if (state.hasReachedMax) return;
        if (state.status == NotificationStatus.initial) {
          try {
            final books = await _repo.getNotification(page: page);

            page = page + 1;
            return emit(const NotificationState().copyWith(
              status: NotificationStatus.success,
              listBooks: books,
              hasReachedMax: false,
            ));
          } catch (e) {
            return emit(const NotificationState().copyWith(
              error: e.toString(),
              status: NotificationStatus.failure,
            ));
          }
        }

        if (page <= limit) {
          try {
            final books = await _repo.getNotification(page: page);
            page = page + 1;
            return emit(const NotificationState().copyWith(
              status: NotificationStatus.success,
              listBooks: List.of(state.listBooks)..addAll(books),
              hasReachedMax: false,
            ));
          } catch (e) {
            return emit(const NotificationState().copyWith(
              error: e.toString(),
              status: NotificationStatus.failure,
            ));
          }
        }

        emit(state.copyWith(hasReachedMax: true));
      },
      transformer: throttleDroppable(throttleDuration),
    );
  }
}
