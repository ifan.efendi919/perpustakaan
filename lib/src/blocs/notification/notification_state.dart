part of 'notification_bloc.dart';

enum NotificationStatus { initial, success, failure }

class NotificationState extends Equatable {
  const NotificationState({
    this.status = NotificationStatus.initial,
    this.listBooks = const <NotificationModel>[],
    this.hasReachedMax = false,
    this.error = '',
  });

  final NotificationStatus status;
  final List<NotificationModel> listBooks;
  final bool hasReachedMax;
  final String error;

  NotificationState copyWith({
    NotificationStatus? status,
    List<NotificationModel>? listBooks,
    bool? hasReachedMax,
    String? error,
  }) {
    return NotificationState(
      status: status ?? this.status,
      listBooks: listBooks ?? this.listBooks,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      error: error ?? this.error,
    );
  }

  @override
  String toString() {
    return '''NotificationState { status: $status, hasReachedMax: $hasReachedMax, Notifications: ${listBooks.length} }, Error : $error''';
  }

  @override
  List<Object> get props => [status, listBooks, hasReachedMax, error];
}