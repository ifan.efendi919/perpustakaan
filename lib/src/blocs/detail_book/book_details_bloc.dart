import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perpustakaan/src/data/models/book_model.dart';
import 'package:perpustakaan/src/data/models/rent_model.dart';

import '../../data/repositories/book_repo_impl.dart';


part 'book_details_event.dart';
part 'book_details_state.dart';

class BookDetailsBloc extends Bloc<BookDetailsEvent, BookDetailsState> {
  final BookRepository _repo;

  BookDetailsBloc(this._repo) : super(BookDetailsInitial()) {
    on<LoadBookDetails>((event, emit) async {
      try {
        emit(BookDetailsLoading());
        BookModel book = await _repo.getBookDetails(id: event.id);
        RentBook? rent = await _repo.getRentBook(id: event.id);
        emit(BookDetailsSuccess(book: book, rent: rent));
      } catch (e) {
        emit(BookDetailsError(e.toString()));
      }
    });
  }
}
