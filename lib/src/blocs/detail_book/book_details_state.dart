part of 'book_details_bloc.dart';

abstract class BookDetailsState extends Equatable {
  const BookDetailsState();

  @override
  List<Object> get props => [];
}

class BookDetailsInitial extends BookDetailsState {}

class BookDetailsLoading extends BookDetailsState {}

class BookDetailsSuccess extends BookDetailsState {
  final BookModel book;
  final RentBook? rent;

  const BookDetailsSuccess({required this.book, required this.rent});

  @override
  List<Object> get props => [book, rent ?? []];
}

class BookDetailsError extends BookDetailsState {
  final String error;

  const BookDetailsError(this.error);

  @override
  List<Object> get props => [error];
}
