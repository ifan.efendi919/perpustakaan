part of 'book_details_bloc.dart';

abstract class BookDetailsEvent extends Equatable {
  const BookDetailsEvent();

  @override
  List<Object> get props => [];
}

class LoadBookDetails extends BookDetailsEvent {
  final int id;

  const LoadBookDetails(this.id);

  @override
  List<Object> get props => [id];
}
